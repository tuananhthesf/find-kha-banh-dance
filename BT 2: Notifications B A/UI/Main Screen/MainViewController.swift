//
//  MainViewController.swift
//  BT 2: Notifications B A
//
//  Created by Nguyen Tuan Anh on 1/22/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBAction func btnBayLuonOnClick(_ sender: UIButton) {
        let questView = QuestionViewController(nibName: "QuestionViewController", bundle: nil)
        self.navigationController?.pushViewController(questView, animated: true)
    }
    
    @IBAction func btnThoatOnClick(_ sender: UIButton) {
        UIControl().sendAction(#selector(NSXPCConnection.suspend),
                               to: UIApplication.shared, for: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: .didReceiveData, object: nil)
    }
    
    @objc func onDidReceiveData(_ notification: Notification) {
        
        if let data = notification.userInfo as? [String: String]
        {
            for (_, score) in data
            {
                if score == "false" {
                    let alert = UIAlertController(title: "Xin lỗi", message: "Ban không phải dân bay lắk rùi", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Về học múa quạt", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Xin chúc mừng", message: "Bạn là dân bay lắk chính hiệu", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Bay luôn", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
}

extension Notification.Name {
    static let didReceiveData = Notification.Name("didReceiveData")
    static let didCompleteTask = Notification.Name("didCompleteTask")
}
