//
//  QuestionViewController.swift
//  BT 2: Notifications B A
//
//  Created by Nguyen Tuan Anh on 1/22/19.
//  Copyright © 2019 Nguyen Tuan Anh. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController {
    
    @IBAction func btnBale(_ sender: UIButton) {
        let score = ["bale":"false"]
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didReceiveData"), object: nil, userInfo: score)
    }
    
    @IBAction func btnSlav(_ sender: UIButton) {
        let score = ["slav":"false"]
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didReceiveData"), object: nil, userInfo: score)
    }
    
    @IBAction func btnHipHop(_ sender: UIButton) {
        let score = ["hiphop":"false"]
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didReceiveData"), object: nil, userInfo: score)
    }
    
    @IBAction func btnBayLac(_ sender: UIButton) {
        let score = ["baylac":"true"]
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didReceiveData"), object: nil, userInfo: score)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
    }

}
